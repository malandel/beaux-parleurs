@extends('layouts.app')

@section('title', 'Success')

@section('content')

<div class="container text-center">
    <p> Bien joué ! Les informations sont ajoutées à la base de données.</p>
    <p class="text-danger"><strong> Attention, si vous rechargez cette page, les données seront insérées de nouveau.</strong></p>
    <p>Nous travaillons sur ce problème. Vous pouvez reprendre la navigation grâce au menu du site.</p>
    <p> Bonne journée !</p>
    <img src="https://media.giphy.com/media/XreQmk7ETCak0/giphy.gif" alt="gif success">

</div>

@endsection