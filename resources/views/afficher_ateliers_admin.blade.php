@extends('layouts.app')

@section('title', 'Ateliers')

@section('content')

<div class="container">
    <h2 class="text-center">Les ateliers</h2>
    <div class="bouton text-center my-4">
        <a href="{{route('ajouter-atelier')}}" class="btn btn-dark">Ajouter un atelier</a>
    </div>

    <ul>
        @foreach ($ateliers as $atelier)
        <li>{{$atelier->date}} - {{$atelier->lieu}} à {{$atelier->horaires}} - durée : {{$atelier->durée_heure}} h - statut : {{$atelier->statut}}</li>
        @endforeach
    </ul>

</div>



@endsection