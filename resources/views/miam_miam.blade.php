@extends('layouts.app')

@section('title', 'Alimenter la roulette')

@section('content')


<div class="container">

    <h2 class="text-center my-4">Alimentez la roulette !</h2>

    <p>Donnez des phrases pour alimenter notre roulette en situations déjantées. Nous les utiliserons lors de nos battles du jeudi !</p>
    <p>Retrouvez des exemples dans la page <a href="{{route('sujets_liste')}}">"Les sujets à venir et passés"</a> ou rejoignez-nous !</p>

    @if(Auth::user())

    <form method="POST" action="/success" class="text-center my-5">
        @csrf
        <input type="text" name="roulette" placeholder="Donnez-moi à manger !">
        <button type="submit" class="btn btn-dark">Crounch</button>
    </form>

    @else
    <p class="my-5 text-danger"><strong>Vous devez être enregistré.e et connecté.e pour pouvoir nourrir la roulette. On ne voudrait pas que n'importe qui lui donne n'importe quoi non plus.</strong></p>

    @endif
</div>


@endsection