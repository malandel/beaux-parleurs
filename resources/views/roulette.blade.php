@extends('layouts.app')

@section('title', 'Roulette')

@section('content')

<h2 class="text-center m-3">{{$sujet->sujet}}</h2>

@if(Auth::user())
    <form action="{{ $sujet -> id }}" method="POST" class="text-center">
        <a href={{"roulette/".$sujet['id']}} class="btn btn-dark my-4">Sujet clos</a>
    </form>
@endif

@endsection