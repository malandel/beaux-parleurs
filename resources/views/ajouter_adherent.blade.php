@extends('layouts.app')

@section('title', 'Ajouter un adhérent')

@section('content')

<div class="container">
    <h2 class="text-center">Ajouter un.e adhérent.e</h2>
    <form action="/admin/success" class="d-flex flex-column mx-auto" style="width:50%;">
    @csrf
        <label for="nom">Nom</label>
        <input type="text" name="nom" placeholder="Nom"> 
        <label for="prenom">Prénom</label>
        <input type="text" name="prenom" placeholder="Prénom">
        <label for="email">Email</label>
        <input type="email" name="email" placeholder="Email">
        <label for="adresse">Adresse</label>
        <input type="text" name="adresse" placeholder="Adresse">
        <label for="telephone">Téléphone</label>
        <input type="phone" name="telephone" placeholder="Téléphone"> 
        <label for="statut">Statut</label>
        <input type="text" name="statut" placeholder="Statut">
        <label for="date_cotisation">Date de cotisation</label>
        <input type="text" name="date_cotisation" placeholder="Date de la dernière cotisation"> 
        <button type="submit" class="btn btn-dark my-4">Valider</button>
    </form>

</div>

@endsection