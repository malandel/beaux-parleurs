@extends('layouts.app')

@section('title', 'Ajouter un atelier')

@section('content')

<div class="container">
    <h2 class="text-center">Ajouter un atelier</h2>
    <form action="/admin/success-atelier" class="d-flex flex-column mx-auto" style="width:50%;">
    @csrf
        <label for="lieu">Lieu</label>
        <input type="text" name="lieu" placeholder="Lieu"> 
        <label for="date">Date</label>
        <input type="date" name="date" placeholder="Date">
        <label for="horaires">Heure de début</label>
        <input type="horaires" name="horaires" placeholder="Heure de début">
        <label for="duree">Durée (en heures)</label>
        <input type="number" name="duree" placeholder="Durée (en heures)">
        <button type="submit" class="btn btn-dark my-4">Valider</button>
    </form>

</div>

@endsection