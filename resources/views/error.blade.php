@extends('layouts.app')

@section('title', 'ERROR')

@section('content')

<div class="container text-center">
    <h2 class="text-danger">Oh non :'( Le site a rencontré un erreur</h2>
    <img src="https://media.giphy.com/media/Qfo0gkQUepOm2TY3Ny/giphy.gif" alt="Un GIF de magicien">
</div>
@endsection