@extends('layouts.app')

@section('title', 'Adhérents')

@section('content')

<div class="container">
    <div class="bouton text-center">
        <a href="{{route('ajouter-adherents')}}" class="btn btn-dark">Ajouter un.e adhérent.e</a>
    </div>
    <div class="d-flex flex-wrap justify-content-between">
        <div class="honneur">
            <h3 class="text-center mt-4">Membres d'honneur : </h3>
            @foreach ($adherents as $adherent)
            @if($adherent->statut === 'membre d\'honneur')
            <li>
                <strong>{{$adherent->prénom}} - {{$adherent->nom}}</strong>
                </br>tél : {{$adherent->téléphone}} - mail : {{$adherent->email}}
                </br>adresse : {{$adherent->adresse}}
                </br>date de cotisation : {{$adherent->date_cotisation}}
            </li>
            @endif
            @endforeach
        </div>
        <div class="actif">
            <h3 class="text-center mt-4">Membres actifs : </h3>
            @foreach ($adherents as $adherent)
            @if($adherent->statut === 'membre actif')
            <li>
                <strong>{{$adherent->prénom}} - {{$adherent->nom}}</strong>
                </br>tél : {{$adherent->téléphone}} - mail : {{$adherent->email}}
                </br>adresse : {{$adherent->adresse}}
                </br>date de cotisation : {{$adherent->date_cotisation}}
            </li>
            @endif
            @endforeach
        </div>
        <div class="bureau">
            <h3 class="text-center mt-4">Membres du bureau : </h3>
            @foreach ($adherents as $adherent)
            @if($adherent->statut === 'bureau')
            <li>
                <strong>{{$adherent->prénom}} - {{$adherent->nom}}</strong>
                </br>tél : {{$adherent->téléphone}} - mail : {{$adherent->email}}
                </br>adresse : {{$adherent->adresse}}
                </br>date de cotisation : {{$adherent->date_cotisation}}
            </li>
            @endif
            @endforeach
        </div>
    </div>
</div>





@endsection