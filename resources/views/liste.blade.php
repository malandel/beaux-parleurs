@extends('layouts.app')

@section('title', 'Sujets passés et à venir')

@section('content')

<div class="container d-flex flex-wrap justify-content-between">
    <div class="future">
        <h2 class="my-4">Les sujets à venir : </h2>

        @foreach($sujets as $sujet)
            @if($sujet->statut === 'en cours')
                <p><span style="font-size:1.2rem;">" {{$sujet->sujet}}</span> " </br>proposé par : <em>{{$sujet->user->name}}</em> le {{$sujet->created_at}}</p>
            @endif
        @endforeach
    </div>
    <div class="past">
        <h2 class="my-4">Les sujets passés : </h2>

        @foreach($sujets as $sujet)
            @if($sujet->statut === 'clos')
            <p><span style="font-size:1.2rem;">" {{$sujet->sujet}}</span> " <br> proposé par : <em>{{$sujet->user->name}}</em> et  tiré le : {{$sujet->date_tirage}}</p>
            @endif
        @endforeach
    </div>
</div>


@endsection