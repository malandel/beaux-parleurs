<nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
    <div class="container">
        <div class="d-flex flex-wrap align-items-center justify-content-sm-center text-success navbar-brand">
            <a class="" href="{{ url('/') }}">
                <img class="navbar-brand" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRh7R7kclHSPx8hXRqrjhBe8e8M7sHAkhCbbQ&usqp=CAU" alt="logo" style="width:150px;">
            </a>  
            <div class="d-flex flex-wrap text-break">
                <p>Les Beaux Parleurs </p>
                <p>  -  </p>
                <p> théâtre d'improvisation</p> 
            </div>    
        </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto">
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link nav-link-1 active text-success" href="/">Les Beaux Parleurs</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle text-success" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                        Sujets d'impros
                    </a>
                    <div class="dropdown-menu">
                      <a class="dropdown-item" href="{{route('roulette')}}">Roulette</a>
                      <a class="dropdown-item" href="{{route('miam')}}">Alimenter la roulette</a>
                      <a class="dropdown-item" href="{{route('sujets_liste')}}">Les sujets à venir & passés</a>
                    </div>
                </li>
                @if (Auth::user())
                    @if(Auth::user()->statut === "admin")
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle text-success" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                            Admin
                        </a>
                        <div class="dropdown-menu">
                          <a class="dropdown-item" href="{{route('admin-ateliers')}}">Ateliers</a>
                          <a class="dropdown-item" href="{{route('admin-adherents')}}">Adhérents</a>
                        </div>
                    </li>
                    @endif
                @endif
                    <!-- Authentication Links -->
                    @guest
                    @if (Route::has('login'))
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                </li>
                @endif

                @if (Route::has('register'))
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                </li>
                @endif
                @else
                <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        {{ Auth::user()->name }}
                    </a>

                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                    </div>
                </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>