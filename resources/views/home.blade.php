@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <h2>Bienvenue sur le site de la Société des Beaux Parleurs</h2>
        <p>Nous sommes une compagnie de théâtre d'improvisation, basée à Pimpos-ley-oÿes.</p>
    </div>
    <h3 class="my-3">Nos prochains ateliers : </h3>
    <ul>
        @foreach ($ateliers as $atelier)
        <li>{{$atelier->date}} - {{$atelier->lieu}} à {{$atelier->horaires}} - durée : {{$atelier->durée_heure}} h</li>
        @endforeach
    </ul>
</div>
@endsection