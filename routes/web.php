<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SujetController;
use App\Http\Controllers\AtelierController;
use App\Http\Controllers\AdherentController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// HOME
Route::get('/', [AtelierController::class, 'encours']);

Auth::routes();

// ADMIN
// Route::middleware(['first', 'second'])->group(function () {
    Route::get('/admin/ateliers', [AtelierController::class, 'afficher_admin'])->name('admin-ateliers');
    Route::get('/admin/adherents', [AdherentController::class, 'afficher_admin'])->name('admin-adherents');
    Route::any('/admin/ajout-adherent', function(){return view('ajouter_adherent');})->name('ajouter-adherents');
    Route::any('/admin/success', [AdherentController::class, 'ajouter']);
    Route::any('/admin/ajout-atelier', function(){return view('ajouter_atelier');})->name('ajouter-atelier');
    Route::any('/admin/success-atelier', [AtelierController::class, 'ajouter']);



// });


// ROULETTE
Route::get('/roulette', [SujetController::class, 'roulette'])->name('roulette');
Route::get('/roulette/{id}', [SujetController::class,'update']);

// ALIMENTER ROULETTE
Route::get('/alimenter-roulette', function(){return view('miam_miam');})->name('miam');
Route::any('/success', [SujetController::class, 'miam_success']);

// LISTE SUJETS
Route::get('/past-and-future', [SujetController::class, 'liste'])->name('sujets_liste');


// PAGES D'ERREUR PERSONNALISEE
Route::fallback(function (){return view('error');
});