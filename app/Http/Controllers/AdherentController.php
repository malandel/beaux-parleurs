<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Adherent;
use Illuminate\Support\Facades\DB;


class AdherentController extends Controller
{
    function afficher_admin(){
        return view('afficher_adherents_admin', ['adherents' => Adherent::all()]);
    }

    function ajouter(Request $req){
        DB::table('adherents')->upsert([
            'nom' => $req -> nom,
            'prénom' => $req -> prenom,
            'email' => $req -> email,
            'adresse' => $req -> adresse,
            'téléphone' => $req -> telephone,
            'statut' => $req -> statut,
            'date_cotisation' => $req -> date_cotisation,
            'created_at' => date('Y-m-d')
        ], ['email', 'statut']);
        return view('ajout_success');
    }
}
