<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Sujet;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;


class SujetController extends Controller
{
    function roulette(){
        return view('roulette', ['sujet' => Sujet::where('statut', 'en cours')->inRandomOrder()->first()]);
    }

    function update(Request $req){
        $sujet=Sujet::findOrFail($req -> id);
        $sujet -> statut = "clos";
        $sujet -> date_tirage = date('Y-m-d H:i:s');
        $sujet->save();
        return redirect('/roulette');
    }

    function miam_success(Request $req){
        $input = $req->input('roulette');
        DB::table('sujets')->insertOrIgnore([
            'sujet' => $input,
            'created_at' => date('Y-m-d H:i:s'),
            'user_id' => Auth::user()->id,
        ]);
        return view('miam_success');
    }

    function liste(){
        return view('liste', ['sujets' => Sujet::all()]);
    }
    

}