<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Atelier;
use Illuminate\Support\Facades\DB;


class AtelierController extends Controller
{
    function encours(){
        return view('home', ['ateliers' => Atelier::where('statut', 'en cours')->get()]);
    }

    function afficher_admin(){
        return view('afficher_ateliers_admin', ['ateliers' => Atelier::all()]);
    }

    function ajouter(Request $req){
        DB::table('ateliers')->insert([
            'lieu' => $req -> lieu,
            'date' => $req -> date,
            'horaires' => $req -> horaires,
            'durée_heure' => $req -> duree,
            'created_at' => date('Y-m-d')
        ]);
        return view('ajout_success');
    }
}
