# Site pour La Société des Beaux Parleurs, théâtre d'impro

Dans le cadre d'une évalution de 3 jours pendant la formation "Développeuse Web & Web Mobile" avec Simplon.co (Labège).

*L'association vous sollicite pour créer un outil en ligne, qui permette à l'ensemble des membres du club de proposer des sujets durant la semaine.*

*Une page "roulette" permettrait ensuite, le jour J, de tirer au sort les sujets au fil de la soirée (à chaque chargement de page, un sujet aléatoire est affiché).*

## Installation du projet en local 

**La branche 'master' est paramétrée pour la mise en ligne avec Heroku, la branche 'dev' est paramétrée pour un travail en local.**

- Cloner le projet, installer les dépendences et aller sur la branche 'dev'
```
git clone git@gitlab.com:malandel/beaux-parleurs.git
cd beaux-parleurs
composer install && npm install
git checkout dev
```
- Dans PHPMyAdmin, créer une nouvelle base de données "beaux-parleurs" en 'utf8mb4-unicode-ci'
- Créer un nouveau fichier .env  à la racine du projet en copiant le fichier .env.example, cela peut être exécuté avec cette commande : ```cp .env.example .env```
- Ensuite, renseigner ses informations de connexion à la base de données (PHPMyAdmin) dans le fichier .env : DB_USERNAME=*votre_username* , DB_PASSWORD=*votre_password*
- Générer votre APP KEY : ```php artisan key:generate```
- Lancer les migrations pour créer les tables, puis lancer les seed : ```php artisan migrate && php artisan db:seed```
- Lancer le serveur : ```php artisan serve``` pour visualiser l'application.
- Créer un compte sur le site pour profiter de toutes ses fonctionnalités. Les codes d'accès admin sont cachés dans le code source ...

## Adresse en ligne

https://beaux-parleurs.herokuapp.com/

## Version en ligne des diagrammes

[UML](https://app.genmymodel.com/api/projects/_tr2CQHTzEeuCM8KqVoRWiA/diagrams/_tr2pUnTzEeuCM8KqVoRWiA/svg)

## Kanban Board

[Trello](https://trello.com/b/QyN8XsDv/eval-impro)


## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell via [taylor@laravel.com](mailto:taylor@laravel.com). All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
