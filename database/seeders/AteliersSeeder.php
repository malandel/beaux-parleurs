<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AteliersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ateliers')->insert([
            'lieu' => 'Salle des Fêtes de Pimpos-ley-Oÿes',
            'date' => '2021-02-03',   
            'horaires' => '18h30',
            'durée_heure' => 2,
            'created_at' =>  date('Y-m-d'),
            'statut' => 'annulé'   
        ]);
        DB::table('ateliers')->insert([
            'lieu' => 'Salle des Fêtes de Pimpos-ley-Oÿes',
            'date' => '2020-02-05',   
            'horaires' => '17h30',
            'durée_heure' => 2,
            'created_at' =>  date('Y-m-d'),
            'statut' => 'terminé' 
        ]);
        DB::table('ateliers')->insert([
            'lieu' => 'Salle municipale de Canaillou-sur-Garonne',
            'date' => '2022-04-03',   
            'horaires' => '18h30',
            'durée_heure' => 2.5,
            'created_at' =>  date('Y-m-d')   
        ]);
    }
}
