<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AdherentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('adherents')->insert([
            'nom' => 'Orange',
            'prénom' => 'Jude',   
            'email' => 'paquito@nectarsucré.com',
            'téléphone' => '04-78-95-67-43',
            'adresse' =>  '1 rue des Fleurs de Printemps, 45890 LaForêtEnchantée',
            'statut' => 'membre d\'honneur',
            'date_cotisation'=> '2021-01-22' ,
            'created_at' =>  date('Y-m-d')
        ]);
        DB::table('adherents')->insert([
            'nom' => 'Nosaure',
            'prénom' => 'Tirah',   
            'email' => 'jurassik@gmail.com',
            'téléphone' => '05-70-35-20-73',
            'adresse' =>  '345 avenue de la Météorite, 48888 Crétacé',
            'statut' => 'bureau',
            'date_cotisation'=> '2021-01-22' ,
            'created_at' =>  date('Y-m-d')
        ]);
        DB::table('adherents')->insert([
            'nom' => 'Stirith',
            'prénom' => 'Mina',   
            'email' => 'ouestefolde@boromir.com',
            'téléphone' => '03-09-78-45-99',
            'adresse' =>  '78 boulevard du Rohan, 67378 Gondor',
            'date_cotisation'=> '2021-01-22' ,
            'created_at' =>  date('Y-m-d')
        ]);
    }
}
