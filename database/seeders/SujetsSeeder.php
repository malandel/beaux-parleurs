<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SujetsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sujets')->insert([
            'sujet' => 'Un coiffeur cosmique prend ses vacances à Londres',
            'created_at' => date('Y-m-d H:i:s'),      
        ]);
        DB::table('sujets')->insert([
            'sujet' => 'Une épidémie de proutalogie éclot à la piscine lors de l\'activité des Maternelles',
            'created_at' => date('Y-m-d H:i:s'),      
        ]);
        DB::table('sujets')->insert([
            'sujet' => 'Deux chats de gouttière deviennent millionnaires',
            'created_at' => date('Y-m-d H:i:s'),         
        ]);
        DB::table('sujets')->insert([
            'sujet' => 'Un fermier vend au marché sa nouvelle race de chèvres',
            'created_at' => date('Y-m-d H:i:s'),            
        ]);
        DB::table('sujets')->insert([
            'sujet' => 'Dans un monde habité par des ustensiles de cuisine, la râpe à fromage prépare un putsch...',
            'created_at' => date('Y-m-d H:i:s'),            
        ]);
        DB::table('sujets')->insert([
            'sujet' => 'Les gardes de la Reine d\'Angleterre discutent de leur cours de macramé',
            'created_at' => date('Y-m-d H:i:s'),             
        ]);
        DB::table('sujets')->insert([
            'sujet' => 'Lors d\'un concert de Lady Gaga, des petits beurres contaminés sèment la panique !' ,
            'created_at' => date('Y-m-d H:i:s'),             
        ]);
    }
}
